import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import { fetchAgeCategories, fetchAgeCategoryInfo } from "../../store/actions";

class AgeForm extends Component {
  state = {
    age: "",
    categoryInfo: null
  };

  componentDidMount() {
    this.props.fetchAgeCategories();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.categoryInfo && prevProps.categoryInfo !== this.props.categoryInfo) {
      this.setState({
        categoryInfo: `Id: ${this.props.categoryInfo.id} Name: ${this.props.categoryInfo.name}`
      });
    }
  }

  inputChangeHandler = event => {
    const { value } = event.target;

    if ((!isNaN(value) && value > 0 && value <= 100) || value === "") {
      this.setState({ age: event.target.value });
    }
  };

  getAgeCategory = age => {
    const { categories } = this.props;

    const category = categories.find(category => {
      const [min, max] = category.name.split("-");

      return age >= min && age <= max;
    });

    if (category) {
      return category;
    } else {
      const categoriesAges = categories.reduce((arr, category) => {
        const categoryAgesArray = category.name.split("-");
        return [...arr, ...categoryAgesArray];
      }, []);

      categoriesAges.sort((a,b) => a - b);

      if (age < categoriesAges[0]) {
        return `Ваш возраст меньше минимального ${categoriesAges[0]} в категориях`;
      }

      if (age > categoriesAges[categoriesAges.length - 1]) {
        return `Ваш возраст больше максимального ${categoriesAges[categoriesAges.length - 1]} в категориях`;
      }
    }
  };

  formSubmitHandler = event => {
    event.preventDefault();

    const { age } = this.state;

    if (age.length > 0) {
      const category = this.getAgeCategory(age);

      if (typeof category === "object") {
        this.props.fetchAgeCategoryInfo(category.id);
      } else {
        this.setState({ categoryInfo: category });
      }
    } else {
      this.setState({ categoryInfo: null });
    }
  };

  render() {
    return (
      <Fragment>
        <form onSubmit={this.formSubmitHandler}>
          <label>Ваш возраст (1-100): </label>
          <input type="text" value={this.state.age} onChange={this.inputChangeHandler}/>
          <button>Проверить</button>
        </form>
        <div>{this.props.categoryInfoLoading ? "Loading..." : this.state.categoryInfo}</div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.categories,
  categoryInfo: state.categoryInfo,
  categoryInfoLoading: state.categoryInfoLoading
});

const mapDispatchToProps = dispatch => ({
  fetchAgeCategories: () => dispatch(fetchAgeCategories()),
  fetchAgeCategoryInfo: categoryId => dispatch(fetchAgeCategoryInfo(categoryId))
});

export default connect(mapStateToProps, mapDispatchToProps)(AgeForm);