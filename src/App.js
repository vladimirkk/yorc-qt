import React from 'react';
import './App.css';
import AgeForm from "./components/AgeForm/AgeForm";

const App = () => (
  <div className="App">
    <AgeForm/>
  </div>
);

export default App;
