import axios from "axios";

export const FETCH_AGE_CATEGORIES_SUCCESS = "FETCH_AGE_CATEGORIES_SUCCESS";

const fetchAgeCategoriesSuccess = categories => ({ type: FETCH_AGE_CATEGORIES_SUCCESS, categories });

export const fetchAgeCategories = () => {
  return dispatch => {
    return axios.get("/age_categories").then(
      response => dispatch(fetchAgeCategoriesSuccess(response.data.data))
    );
  }
};

export const FETCH_AGE_CATEGORY_INFO_REQUEST = "FETCH_AGE_CATEGORY_INFO_REQUEST";
export const FETCH_AGE_CATEGORY_INFO_SUCCESS = "FETCH_AGE_CATEGORY_INFO_SUCCESS";
export const FETCH_AGE_CATEGORY_INFO_FAILURE = "FETCH_AGE_CATEGORY_INFO_FAILURE";

const fetchAgeCategoryInfoRequest = () => ({ type: FETCH_AGE_CATEGORY_INFO_REQUEST });
const fetchAgeCategoryInfoSuccess = categoryInfo => ({ type: FETCH_AGE_CATEGORY_INFO_SUCCESS, categoryInfo });
const fetchAgeCategoryInfoFailure = () => ({ type: FETCH_AGE_CATEGORY_INFO_FAILURE });

export const fetchAgeCategoryInfo = categoryId => {
  return dispatch => {
    dispatch(fetchAgeCategoryInfoRequest());

    return axios.get(`/age_categories/${categoryId}`).then(
      response => dispatch(fetchAgeCategoryInfoSuccess(response.data)),
      () => dispatch(fetchAgeCategoryInfoFailure())
    );
  }
};