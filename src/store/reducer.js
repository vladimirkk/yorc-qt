import {
  FETCH_AGE_CATEGORIES_SUCCESS,
  FETCH_AGE_CATEGORY_INFO_REQUEST,
  FETCH_AGE_CATEGORY_INFO_SUCCESS,
  FETCH_AGE_CATEGORY_INFO_FAILURE
} from "./actions";

const initialState = {
  categories: null,
  categoryInfo: null,
  categoryInfoLoading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_AGE_CATEGORIES_SUCCESS:
      return { ...state, categories: action.categories };
    case FETCH_AGE_CATEGORY_INFO_REQUEST:
      return  {...state, categoryInfoLoading: true, categoryInfo: null };
    case FETCH_AGE_CATEGORY_INFO_SUCCESS:
      return  {...state, categoryInfoLoading: false, categoryInfo: action.categoryInfo };
    case FETCH_AGE_CATEGORY_INFO_FAILURE:
      return  {...state, categoryInfoLoading: false };
    default:
      return state;
  }
};

export  default reducer;